Ejercicio 1: Implementación de las soluciones al ejercicio 2 del práctico de la semana 4.

En la carpeta Algoritmos2.2017/semana04/lab/ejercicios/1
disponés de los siguientes archivos:

README1.txt : es el archivo que estás viendo.
array_helpers.h : contiene descripciones de funciones para cargar y volcar al disco los datos climáticos.
array_helpers.c : contiene implementaciones de dichas funciones, la de cargar está incompleta.
main.c : contiene al programa principal que invoca al procedimiento de carga de los datos
         climáticos e inmediatamente procede a volcarlos por la salida estándard.

Abrí el archivo ../input/weather_cordoba.in para ver cómo vienen los datos climáticos.
Cada línea contiene las mediciones realizadas en un día.
Las primeras 3 columnas corresponden al año, mes y día de las mediciones. Las restantes
6 columnas son la temperatura media, la máxima, la mínima, la presión atmosférica, la
humedad y las precipitaciones medidas ese día.
Para evitar los números reales, los grados están expresados en décimas de grados (por
ejemplo, 15.2 grados está representado por 152 (décimas)). La presión también ha sido
multiplicada por 10 y las precipitaciones por 100, o sea que están expresadas en
centésimas de milímetro). Esto permite representar todos los datos con números enteros.
Vos no necesitás multiplicar ni dividir estos valores, esta información es sólo para
que puedas entender los datos.

Lo primero que tenés que hacer es completar el procedimiento de carga de datos en el
archivo array_helpers.c. Además de observar el resto del archivo array_helpers.c para
entenderlo, podés revisar el mismo archivo de la primera semana del lab. Tenes que
completar donde dice "PLEASE COMPLETE".

Una vez que lo hayas hecho podés testear si la carga funciona correctamente ejecutando:

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o weather *.o main.c
./weather ../input/weather_cordoba.in > weather_cordoba.out

si no hubo ningún error, ahora podés comparar la entrada con la salida:

diff ../input/weather_cordoba.in weather_cordoba.out

si esto último no arroja ninguna diferencia, significa que tu carga funciona correctamente.
En tal caso, creá los archivos weather.h y weather.c y resolvé cada uno de los incisos
del ejercicio 2 del práctico de esta semana. Modificá también main.c (y array_helpers.h (y .c))
para que se desplieguen los resultados obtenidos en cada inciso.


