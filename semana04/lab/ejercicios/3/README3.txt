Ejercicio 3: El mismo ejercicio 2, ahora es un arreglo de PUNTEROS a tuplas.

La intención del ejercicio es explorar cómo afecta la performance de la ordenación
que el arreglo contenga punteros a tuplas, en vez de las propias tuplas.

En la carpeta Algoritmos2.2017/semana04/lab/ejercicios/3
disponés de los siguientes archivos:

README3.txt : es el archivo que estás viendo.
helpers.h : contiene descripciones de funciones para cargar y volcar al disco datos
            de los jugadores del tenis profesional, etre otras.
helpers.c : contiene implementaciones de dichas funciones.

Primero compará helpers.h y helpers.c con los del ejercicio anterior. 

diff helpers.h ../2/helpers.h
diff helpers.c ../2/helpers.c

¿Cuáles son las diferencias?

El ejercicio es realizar los cambios que necesites para ordenar el nuevo arreglo con
el mismo algoritmo que en el ejercicio anterior, conforme a los cambios ya realizados
en los dos archivos dados.

El ejercicio se resuelve copiando los archivos sort.c sort.h y main.c del ejercicio
anterior y cambiando lo que sea necesario en esos archivos para el correcto funcionamiento
del algoritmo de ordenación.

Una vez logrado eso, compará los tiempos de utilización de cpu de las soluciones
a los ejercicios 2 y 3. Para ello, tenés que ejecutar varias veces cada uno puesto
que los tiempos obtenidos no son siempre idénticos.

¿Hay alguna diferencia en los tiempos de ejecución de uno y otro algoritmo?
¿A qué se debe?
