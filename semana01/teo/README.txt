En la carpeta Algoritmos2.2017/semana01/teo
disponés de los siguientes archivos:

README.txt : es el archivo que estás viendo.

2017.01.ordenación.elemental.filminas.pdf : son las filminas de la primeras dos clases.
2017.01.ordenación.elemental.imprimible.pdf : lo mismo pero abreviado para impresión.
2017.01.ordenación.elemental.cuestionario.pdf : preguntas para estudiar estas dos clases.

Contenidos de la primera clase :

- muy breve descripción de la organización de la materia, referencia a la wiki
- qué vs cómo
- problema motivador : el problema del bibliotecario
- ¿qué significa que algo esté ordenado? ¿cómo se verifica?
- ¿cómo ordenar?
- ordenación por selección :
  - idea
  - ejemplo
  - invariantes
  - algoritmo con ciclos do
- el ciclo for
  - caso particular del ciclo do
  - restricciones
    - indice no se modifica en el cuerpo,
    - se incrementa (o decrementa en el caso del downto) una constante
    - nace y muere en el for,
    - al comienzo de la ejecución del for se puede establecer precisamente el número
    de veces que se ejecutará su cuerpo
- ordenación por selección con for en vez de do
- análisis de la ordenación por selección
  - operación representativa : definición
  - elección de operación representativa : la comparación
  - respuesta al problema del bibliotecario
- ops : contando cuántas veces se realiza una operación
  - ops de una secuencia
  - ops de skip
  - ops del ciclo for
  - ops del condicional
  - ops de la asignación
  - ops de una expresión
  - ops del do queda para la segunda clase!!
- ejemplo : contando las comparaciones de la ordenación por selección

Contenidos de la segunda clase :
- repaso
- ordenación por inserción :
  - idea
  - ejemplo
  - invariantes
  - algoritmo
- análisis
  - número de comparaciones
  - número de swaps
  - mejor caso, peor caso y caso medio
  - significado de cada caso
  - respuesta al problema del bibliotecario
- comparación y conclusiones
  - comparación entre los dos algoritmos vistos
  - demo
  - discusión sobre paralelismo

