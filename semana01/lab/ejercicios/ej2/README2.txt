Ejercicio 2: Ordenar en forma decreciente.

Si estás leyendo esto es porque resolviste el ejercicio 1, y alguno de los profes te
dio el ok para seguir con el 2. En la carpeta Algoritmos2.2017/semana01/lab/ejercicios/ej2
disponés del siguiente archivo:

README2.txt : es el archivo que estás viendo.

para continuar, copiá los archivos array_helpers.h, array_helpers.c, sort.h, sort.c y main.c
del ejercicio 1. El sort.c tiene tu implementación de la función array_is_sorted.

Para copiarlos podés hacer
(atención: tené cuidado de copiar la línea entera inclusive el ultimo punto)

cp ../ej1/*.h .
cp ../ej1/*.c .

No te olvides de cerrar los archivos en los que estabas trabajando durante el ejercicio 1
para no confundirte con los de este ejercicio; los archivos tienen los mismos nombres.

Este ejercicio consiste en modificar lo que haga falta en el archivo sort.c para que el
programa ordene de mayor a menor en lugar de hacerlo de menor a mayor.

Como en el ejercicio 1, tenés que compilar ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort.c

y revisando tu código hasta que no haya errores.
Y luego ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c

y corriendo el programa con la ejecución de

./sorter ../input/example-unsorted.in

Si anda bien, probá con otros archivos de la carpeta ../input

¿no sería lógico cambiar los nombres de algunas variables y/o funciones auxiliares
que fueron elegidos para cuando el arreglo se ordenaba crecientemente?
Hacélo y comprobá que el programa siga compilando y funcionando.

Mostrale lo que hiciste a alguno de los profes y luego pasá al ejercicio 3 haciendo

cd ../ej3

y abrí el archivo README3.txt

