Ejercicio 1: Implementación de la función array_is_sorted

En la carpeta Algoritmos2.2017/semana01/lab/ejercicios/ej1
disponés de los siguientes archivos:

README1.txt : es el archivo que estás viendo.
array_helpers.h : contiene descripciones de funciones auxiliares para manipular arreglos.
array_helpers.c : contiene implementaciones de dichas funciones.
sort.h : contiene descripciones de las funciones selection_sort y array_is_sorted
sort.c : contiene una implementación de selection_sort pero no de array_is_sorted
main.c : contiene al programa principal que carga un arreglo de números,
         luego lo ordena con la función selection_sort y finalmente comprueba que
         el arreglo sea permutación ordenada del que cargó inicialmente.

Para este último paso es necesario que abras el archivo sort.c e implementes la función
array_is_sorted. Para guiarte, no dudes en estudiar el resto del archivo sort.c,
principalmente la implementación del selection sort, y la definición del algoritmo
array_is_sorted que hiciste durante el práctico del lunes.

Una vez implementada la función array_is_sorted, compilá ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort.c

Si reporta algún error, revisá tu código y volvé a ejecutar la línea de arriba.
Una vez que esa línea se ejecuta sin reportar errores, ejecutá

gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c

y ya podés correr el programa, ejecutando

./sorter ../input/example-unsorted.in

Si anda bien, probá con otros archivos de la carpeta ../input
no te olvides de probar con el archivo ../input/empty.in

Mostrale lo que hiciste a alguno de los profes y luego pasá al ejercicio 2 haciendo

cd ../ej2

y abrí el archivo README2.txt

