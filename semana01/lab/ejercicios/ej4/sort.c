#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort.h"

static bool goes_before_than(int x, int y){
        bool v=((abs(x)<abs(y)) || (abs (x)==abs(y) && ((x<0 && y>0) || (x==y))));
	return v;
}

static void swap(int a[], unsigned int i, unsigned int j) {
    int tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
}

bool array_is_sorted(int array[], unsigned int length) {
	bool x=true;
	unsigned int i=1;
 	while (i+1<length && x){
		if (goes_before_than(array[i-1],array[i])){
			i++;
		}
		else{
			x=false;
		}
	}
	return x;
}


static unsigned int min_pos_from(int a[], unsigned int i, unsigned int length) {
    unsigned int min_pos = i;
    for (unsigned int j = i + 1;j < length; j++){
        if (goes_before_than(a[j], a[min_pos])) {
            min_pos = j;
        }
    }
    return (min_pos);
}

void selection_sort(int a[], unsigned int length) {
    for (unsigned int i = 0; i<length; i++) {
        unsigned int min_pos = min_pos_from(a, i, length);
    swap(a, i, min_pos);
    }
}


