Ejercicio 1: Implementación del algoritmo de ordenación por inserción

En la carpeta Algoritmos2.2017/semana02/lab/ejercicios/ej1
disponés de los siguientes archivos:

README1.txt : es el archivo que estás viendo.
array_helpers.h : contiene descripciones de funciones auxiliares para manipular arreglos.
array_helpers.c : contiene implementaciones de dichas funciones.
sort_helpers.h : contiene descripciones de goes_before, array_is_sorted y swap
sort_helpers.o : contiene implementaciones ilegibles de esas funciones (código compilado)
sort.h : contiene descripción de la función insertion_sort
sort.c : contiene una implementación incompleta de insertion_sort, falta implementar insert
main.c : contiene el programa principal que carga un arreglo de números,
         luego lo ordena con la función insertion_sort y finalmente comprueba que
         el arreglo sea permutación ordenada del que cargó inicialmente.

Para que logre odenar dicho arreglo es necesario que abras el archivo sort.c e implementes el
procedimiento insert. Para guiarte, no dudes en examinar el resto del archivo sort.c y la definición del algoritmo de ordenación por inserción que hemos visto en clase. El algoritmo
debe ordenar con respecto a la relación goes_before, provista por sort_helpers.h

Una vez implementado el procedimiento insert, compilá ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c

y ya podés correr el programa, ejecutando

./sorter ../input/example-unsorted.in

Si anda bien (o sea, si no reporta error) probá con otros archivos de la carpeta ../input
no te olvides de probar con el archivo ../input/empty.in

¿Te das cuenta qué relación implementa la función goes_before?

Si todo anda bien, pasá al ejercicio 2, haciendo

cd ../ej2

y abrí el archivo README2.txt

