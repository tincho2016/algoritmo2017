#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"


static void quick_sort_rec(int a[], unsigned int izq, unsigned int der) {
	unsigned int piv;
	if (der > izq) {
		piv = partition (a,izq,der);
 		if (piv != 0)
			quick_sort_rec(a,izq,piv-1);
		else
			piv = izq;
 		quick_sort_rec(a,piv+1,der);
	}
}

void quick_sort(int a[], unsigned int length) {
    quick_sort_rec(a, 0, (length == 0) ? 0 : length - 1);
}

