Ejercicio 2: Implementación top-down del algoritmo de ordenación rápida, primera parte

En la carpeta Algoritmos2.2017/semana02/lab/ejercicios/ej2
disponés de los siguientes archivos:

README2.txt : es el archivo que estás viendo.
array_helpers.h : es el mismo que en el ejercicio anterior.
array_helpers.c : también.
sort_helpers.h : contiene además una descripción de partition
sort_helpers.o : contiene implementaciones ilegibles de todo lo descripto en sort_helpers.h
sort.h : contiene descripción de la función quick_sort
sort.c : contiene una implementación muy incompleta de quick_sort,
         falta implementar quick_sort_rec
main.c : contiene el programa principal que carga un arreglo de números,
         luego lo ordena con la función quick_sort y finalmente comprueba que
         el arreglo sea permutación ordenada del que cargó inicialmente.

Para que logre odenar dicho arreglo es necesario que abras el archivo sort.c e implementes el
procedimiento quick_sort_rec. Ojo: no es necesario que implementes la función partition puesto
que la misma ya está implementada (salvo que no podés leer el código porque solo disponés
de la versión compilada). Para saber cómo utilizarla, examiná su descripción en sort_helpers.h.
A modo de guía para implementar quick_sort_rec, no dudes en revisar la presentación que
hicimos del algoritmo de ordenación rápida en clase.

Una vez implementado el procedimiento quick_sort_rec, compilá ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c

y ya podés correr el programa, ejecutando

./sorter ../input/example-unsorted.in

Si anda bien (o sea, si no reporta error) probá con otros archivos de la carpeta ../input
no te olvides de probar con el archivo ../input/empty.in

Mostrale lo que hiciste a alguno de los profes y luego pasá al ejercicio 3 haciendo

cd ../ej3

y abrí el archivo README3.txt

