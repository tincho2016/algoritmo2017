Ejercicio 4: Comparación de todos los algoritmos de ordenación implementados

En la carpeta Algoritmos2.2017/semana02/lab/ejercicios/ej4
disponés de los siguientes archivos:

README4.txt : es el archivo que estás viendo.
sort_helpers.h : contiene además descripciones de manejadores de contadores
sort_helpers.o : contiene implementaciones ilegibles de todo lo descripto en sort_helpers.h
sort.h : contiene descripción de las funciones de ordenación implementadas
sort.c : contiene una implementaciones incompletas de los algoritmos de ordenación trabajados
main.c : contiene el programa principal que carga un arreglo de números,
         luego lo ordena con cada uno de los algoritmos de ordenación implementados
         y muestra el número de comparaciones e intercambios realizados.

Ahora tenés que copiar los archivos array_helpers.h y array_helpers.c de un ejercicio
anterior.

Luego abrí el archivo sort.c y copiar el código de cada uno de los algoritmos de ordenación.

Compilá ejecutando

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c

y ya podés correr el programa, ejecutando

./sorter ../input/example-unsorted.in

Si anda bien (o sea, si no reporta error) probá con otros archivos de la carpeta ../input

Mostrale lo que hiciste a alguno de los profes y luego terminaste con el lab de la segunda
semana.
