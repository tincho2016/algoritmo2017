
merge :: Ord a => [a] -> [a] -> [a]
merge as [] = as
merge [] bs = bs
merge (a:as) (b:bs) = if a <= b then a : merge as (b:bs) else b : merge (a:as) bs

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge sas sbs
         where sas = msort as
               sbs = msort bs
               k = length xs `div` 2
               as = take k xs
               bs = drop k xs

