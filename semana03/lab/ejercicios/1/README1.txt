Ejercicio 1: Implementación de un algoritmo inventado que llamamos ordenación por permutación

En la carpeta Algoritmos2.2017/semana03/lab/ejercicios/1
disponés de los siguientes archivos:

README1.txt : es el archivo que estás viendo.
array_helpers.h : el que conocés de la primera clase.
array_helpers.c : idem.
sort_helpers.h : contiene descripciones de goes_before, array_is_sorted y swap.
sort_helpers.c : contiene sus implementaciones.
sort.h : contiene descripción de la función permutation_sort
sort.c : contiene una implementación incompleta de permutation_sort.
main.c : contiene el programa principal que carga un arreglo de números,
         luego lo ordena con la función permutation_sort y finalmente comprueba que
         el arreglo sea permutación ordenada del que cargó inicialmente.

La idea es ir generando sistemáticamente las permutaciones del arreglo dado y finalizar
cuando se haya obtenido una permutación ordenada. Como puede haber n! permutaciones
posibles del arreglo (¿puede haber menos? ¿puede haber más?) este algoritmo tiene como mínimo
orden n! en el peor caso. ¿Y en el mejor caso?

Por ejemplo, para ordenar (el arreglo a) 2 -1 3 8 0 uno podría generar todas las permutaciones:

2 -1 3 8 0
2 -1 3 0 8
2 -1 8 3 0
2 -1 8 0 3

etc, hasta dar con

0 -1 2 3 8

que es el arreglo ordenado (utilizando la comparación goes_before)

Es bastante complicado generar todas las permutaciones del arreglo a.
Más fácil es dejar ese arreglo como está, y fabricar un nuevo arreglo que llamamos
perm:

0 1 2 3 4             (*)

y ahora sí se vuelve menos complicado generar todas las permutaciones de perm:

0 1 2 3 4             (+)
0 1 2 4 3
0 1 3 2 4
0 1 3 4 2

pero recordemos que el arreglo que queremos que quede ordenado es a, o sea que nos
vamos a detener cuando lleguemos a la siguiente permutación de perm:

4 1 0 2 3

porque mirando a vemos que perm justo dice en qué orden deben ir los elementos de a:
primero debe ir a[4], después a[1], después a[0], luego a[2] y finalmente a[3].

En el archivo sort.c encontrarás la operación next_permutation que dada una permutación
de la secuencia (+), la convierte en la siguiente permutación de esa secuencia.

Tenés que implementar vos la operación mk_fst_permutation que fabrica la permutación (*).

También tenés que implementar la función sorted, que dada la permutación perm y el arreglo
a devuelve verdadero si es cierto que a está ordenado siguiendo el orden indicado por
perm.

Por último, tenés que implementar el procedimiento update que -por única vez- modifica a
para que quede ordenado, aprovechando que perm indica cómo tienen que ubicarse sus elementos.

Como siempre:

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c array_helpers.c sort_helpers.c sort.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o sorter *.o main.c
./sorter ../input/example-unsorted.in

Acordate de probar con otros archivos de la carpeta ../input
fijate que hay archivos nuevos, de arreglos de tamaño 6, 7, 8, 9, etc
andá probando gradualmente con arreglos de tamaño creciente

y no te olvides de probar con el archivo ../input/empty.in


