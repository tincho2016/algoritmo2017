Ejercicio 2: Implementación de un algoritmo para chequear si una proposición es o no una tautología revisando su tabla de verdad

En la carpeta Algoritmos2.2017/semana03/lab/ejercicios/2
disponés de los siguientes archivos:

README2.txt : es el archivo que estás viendo.
prop.h : tipo abstracto de dato (TAD) proposición.
prop.c : implementación del TAD proposición.
prop_helpers.h : contiene descripciones de operaciones auxiliares sobre proposiciones.
prop_helpers.c : implementación de esas operaciones.
evaluation.h : contiene descripciones de la función de evaluación y de la función que
               determina si una proposición es o no una tautología
evaluation.c : contiene una implementación de la función de evaluación, y una implementación
               muy incompleta de la función que determina si una proposición es o no una
               tautología
main.c : contiene el programa principal que carga una proposición y luego comprueba
         si se trata o no de una tautología.

Hay algún parecido con el problema anterior, en que hay que generar todas las posibilidades:
antes eran todas las permutaciones, ahora son todas las líneas de la tabla de verdad.

La idea es ir generando sistemáticamente las líneas de la tabla de verdad, partiendo por ejemplo de false false ... false hasta llegar a true true ... true. Si en el camino se encuentra una línea que no satisface la proposición, se puede detener la ejecución. ¿Cuántas posibilidades hay en el peor caso, asumiendo que hay n letras proposicionales? Eso determinará el orden del algoritmo.

Es necesario generar una primera línea de la tabla. Luego una operación que dada una línea de la tabla obtenga la siguiente y una función que permita determinar cuándo se han explorado todas las líneas.

Por ejemplo, para la proposición (p_2)=>(!(p0)) (siii, lamentablemente todos esos paréntesis
son necesarios por ahora) se considera la siguiente tabla de verdad:

p0 p1 p2 !(p0) (p2)=>(!(p0))
f  f  f    v       v
f  f  v    v       v
f  v  f    v       v
f  v  v    v       v
v  f  f    f       v
v  f  v    f       f

y no hace falta continuar con la tabla porque ya se ve que no es una tautología.

Observar que en la tabla hay 3 letras proposicionales, eso es porque la letra proposicional
con número más grande utilizada es p2. La tabla de verdad de (p3)&(p17) tendría 18 letras proposicionales (desde p0 hasta p17). Esto se puede optimizar, pero no es el objetivo en este
ejercicio. 

Las proposiciones se escriben así:

T (la proposición verdadera)
F (la proposición falsa)
p0 (la proposición que consiste de esa letra proposicional, lo mismo para p1, p2, etc)
!(...) (la proposición que se niega con ! va SIEMPRE entre paréntesis)
(..)&(..) (los dos operandos de la conjunción van SIEMPRE entre paréntesis)
etcétera

Como siempre, para compilar

gcc -Wall -Werror -Wextra -pedantic -std=c99 -c prop_helpers.c prop.c evaluation.c
gcc -Wall -Werror -Wextra -pedantic -std=c99 -o taut *.o main.c
./taut ../input/prop0.in

Acordate de probar con otros archivos con proposiciones de la carpeta ../input
