#include "prop.h"
#include "evaluation.h"


bool eval(prop_t prop, bool line[]) {
/* evaluates the proposition in the given line of the truth table
 * assumes array line has the values for all the propositional letters
 * in the proposition
 */
    bool res;
    if (is_prop_true(prop)) {
        res = true;
    } else if (is_prop_false(prop)) {
        res = false;
    } else if (is_prop_var(prop)) {
        res = line[get_var(prop)];
    } else if (is_prop_not(prop)) {
        res = !eval(get_sub_prop(prop),line);
    } else if (is_prop_and(prop)) {
        res = eval(get_left_prop(prop),line) && eval(get_right_prop(prop),line);
    } else if (is_prop_or(prop)) {
        res = eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_then(prop)) {
        res = !eval(get_left_prop(prop),line) || eval(get_right_prop(prop),line);
    } else if (is_prop_iff(prop)) {
        res = eval(get_left_prop(prop),line) == eval(get_right_prop(prop),line);
    } 
    return (res);
}

static void next_line(bool line[], unsigned int length) {
	int i = length-1;
	bool x=true;
	while (i >= 0 && x){
		if (!line[i])
			x=false;
		line[i] = !line[i];
		i--;
	}
}


static void mk_fst_line(bool line[], unsigned int length) {
	unsigned int i;
	for (i = 0; i < length; i++)
		line[i] = false;
}

static bool last_line(bool line[], unsigned int length) {
	unsigned int i=0;
	while ( i < length && line[i]){
		i++;
}
	return (i >= length);
}



bool is_tautology(prop_t prop) {
	unsigned int length = get_max_var(prop);
	bool line [length];
	mk_fst_line(line, length); 
	while (eval (prop, line) && !last_line (line, length)) {
		next_line (line, length);
	}
	return eval (prop,line);

}


