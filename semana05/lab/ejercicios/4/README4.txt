Ejercicio 4: Especificar TADs en Haskell y utilizarlos para un prototipo que genere números primos.

Para generar los números primos menores que un n dado se nos ocurre el siguiente algoritmo.
En realidad no se nos ocurrió ahora sino hace 2200 años. Tampoco se nos ocurrió a nosotros
sino a Eratóstenes.

Tenemos todos los números entre 2 y n-1. Tomamos el primero (que es el 2) y eliminamos todos
sus múltiplos. Y seguimos de esta manera, tomamos el primero que queda (que ahora es el 3)
y eliminamos sus múltiplos. Tomamos el primero que queda (que es el 5 porque el 4 fue eliminado
por ser múltiplo de 2) y eliminamos todos sus múltiplos. Etc. De esta manera vamos tomando
todos los primos entre 2 y n-1.

Para implementar este algoritmo identificamos que necesitamos Booleanos (para poder
determinar si ser "x es múltiplo de y" es Verdadero o False) y Naturales (por ejemplo,
para poder utilizar los números entre 2 y n-1).

Escribir en un archivo TADBooleano.hs una especificación en Haskell del TAD Booleano,
con los constructores Verdadero y Falso y las operaciones lógicas y, o, no.

Luego escribir en un archivo TADNatural.hs una especificación en Haskell del TAD Natural,
con los constructores Cero y Sucesor, y las operaciones es_cero, predecesor (que se aplica
solamente a naturales que no sean Cero), más, menos (se aplica cuando el segundo argumento
es menor_o_igual al primero) y por. También las operaciones menor_o_igual, es_múltiplo_de.

El prototipo del algoritmo se da en Ejemplo.hs.
Una vez terminados los TADs, podés ejecutarlo:

ghci Ejemplo.hs

y una vez cargado correctamente, evaluar

Completar un prototipo del algoritmo mencionado usando estos TADs.

primos mil

Observación: si definiste correctamente los TADs vas a visualizar los números primos menores
a mil. Se ven "bonitos" porque el prototipo incluye un "visualizador". ¿Querés ver cómo
se vería sin él? Reemplazá el "deriving Eq" por "deriving (Eq, Show)" en la definición
del tipo Natural, y borra el "instance Show Natural ...." de Ejemplo.hs.

Y volvé a cargar y ejecutar "primos mil" ...


